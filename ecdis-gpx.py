import re # Module pour utiliser les expressions régulières (REGEX).
import sys # Pour, entre autres, récupérer les arguments passés au script.

# ----------- Partie 1 ----------
# Parsage du fichier .cvt exporté depuis l'ECDIS.
fichier_ecdis = open(sys.argv[1],'r')

ligne = fichier_ecdis.readline() # Lecture de la première ligne du fichier.
ligne = re.search("^;Route (([\w]+[\s]?[^\w]?[\s]?)+)\s",ligne) # ^ : recherche au début ! On vérifie qu'il s'agit bien d'une route.
if ligne : # S'il s'agit bien d'une route, on récupère son nom et on l'affiche.
	route_nom = ligne.group(1)
	print("Une route a été trouvée : " + route_nom + "\n")

liste_wpt = [] # Liste des wpt.

while 1 :
	wpt = [] # Contient le numéro, le nom et les coordonées du wpt.
	ligne = fichier_ecdis.readline() # On commence à parser les waypoints de la route.
	if re.search("^;",ligne) : # En vérifiant la présence d'un point virgule on s'assure qu'il y a quelque chose à suivre (un WPT).
		wpt_info_1 = fichier_ecdis.readline() # Le numero et le nom du wpt.
		wpt_info_2 = fichier_ecdis.readline() # Les coordonées du wpt.

		num_nom = re.search("^WP ([0-9]{3}) Name (.+)",wpt_info_1) # Format attendu (numéro et nom).
		lat_lon = re.search("^Lat[\s]{2,3}([0-9]{1,2})ø([0-9]{2}.[0-9]{6}[NS]) Lon[\s]{1,3}([0-9]{1,3})ø([0-9]{2}.[0-9]{6}[EW])",wpt_info_2)

		if num_nom and lat_lon : # Conforme au format attendu.
			wpt_numero = num_nom.group(1) # On récupère le numéro du waypoint.
			wpt_nom = num_nom.group(2) # Ainsi que son nom.
			wpt_lat = lat_lon.group(1) + "°" + lat_lon.group(2) # Puis sa latitude.
			wpt_lon = lat_lon.group(3) + "°" + lat_lon.group(4) # Et sa longitude.

			# Affichage dans la console du waypoint trouvé. C'est optionnel, mais peut-être utile pour du débogage.
			#print("wpt trouvé - numero : " + wpt_numero + " - nom : " + wpt_nom + " - latitude : " + wpt_lat + " - longitude : " + wpt_lon)

			wpt.append(wpt_numero) # On ajoute le numéro du wpt.
			wpt.append(wpt_nom) # Puis son nom.
			wpt.append(wpt_lat) # Valeur numérique de la latitude en ° et '.
			wpt.append(wpt_lon) # Valeur numérique de la longitude en ° et '.
			liste_wpt.append(wpt) # Ajout du wpt à la liste.

			fichier_ecdis.readline() # On parcours une ligne qui ne nous intéresse pas.
			fichier_ecdis.readline() # Pareil.

	else : # Quand la ligne ne commence pas par un ";", ou qu'elle ne commence pas du tout !
		print("\nL'intégralité du fichier a été parsé.\n")
		break

fichier_ecdis.close() # On oublie pas de fermer proprement le fichier !

# ---------- Partie 2 ----------
# Conversion des latitudes et longitudes

for i in range(len(liste_wpt)) : # Pour chaque waypoint dans la liste des waypoints ...
	#print("latitude en ° et ' : " + liste_wpt[i][2]) # On (ré)affiche la latitude (utile pour du débogage).
	lat_1 = liste_wpt[i][2]
	lat_2 = re.search("^([0-9]{1,2})°([0-9]{2}.[0-9]{6})([NS])",lat_1)
	lat_3 = round(float(lat_2.group(1)) + float(lat_2.group(2))/60,6) # Conversion de la latitude en ° et ' décimales en ° décimaux.

	if lat_2.group(3) == "S" : # Si la latitude est sud, alors elle est négative (norme du fichier GPX).
		lat_3 = -lat_3

	liste_wpt[i][2] = str(lat_3) # La latitude en ° décimaux remplace la latitude en ° et ' décimales.
	#print("latitude en ° décimaux : " + liste_wpt[i][2] + "\n")

	#print("longitude : " + liste_wpt[i][3]) # On (ré)affiche la longitude (utile pour du débogage).
	lon_1 = liste_wpt[i][3]
	lon_2 = re.search("^([0-9]{1,3})°([0-9]{2}.[0-9]{6})([EW])",lon_1)
	lon_3 = round(float(lon_2.group(1)) + float(lon_2.group(2))/60, 6) # Conversion de la longitude en ° et ' décimales en ° décimaux.

	if lon_2.group(3) == "W" : # Si la longitude est ouest, alors elle est négative (norme du fichier GPX).
		lon_3 = -lon_3

	liste_wpt[i][3] = str(lon_3) # La longitude en ° décimaux remplace la latitude en ° et ' décimales.
	#print("longitude en ° décimaux : " + liste_wpt[i][3] + "\n")

# ---------- Partie 3 ----------
fichier_gpx = open(route_nom+".gpx",'w') # Création du fichier GPX.

fichier_gpx.write( # En-tête standard d'un fichier GPX.
'''<?xml version="1.0"?>
<gpx version="1.1"
	xmlns="http://www.topografix.com/GPX/1/1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<rte>\n''')

fichier_gpx.write("	<name>" + route_nom + "</name>\n") # Nom de la route.

for i in range(len(liste_wpt)) : # Pour chaque waypoint dans la liste des waypoints ...
	fichier_gpx.write("		<rtept lat=\"" + liste_wpt[i][2] + "\" lon=\"" + liste_wpt[i][3] +"\">\n") # ... on écrit ses coordonnées ...
	fichier_gpx.write("			<name>" + liste_wpt[i][1] + "</name>\n") # ... et son nom.
	fichier_gpx.write("		</rtept>\n")

fichier_gpx.write("	</rte>\n") # Quand tous les waypoints ont été écrits, on "ferme" la route.
fichier_gpx.write("</gpx>") # Puis on "ferme" l'ensemble des données GPX.
fichier_gpx.close() # Fermeture propre du fichier.
